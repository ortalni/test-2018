
#------Chart------
install.packages("ggplot2")
library(ggplot2)

#------Linear Model------
install.packages('caTools')
library(caTools)

install.packages('dplyr')
library(dplyr)

#------Missing values------
install.packages('Amelia')
library(Amelia)

#------Correlations------ 
install.packages("corrplot")
library(corrplot)

#------Kmeans algorithm------
install.packages('cluster')
library(cluster)

#------SQL------
install.packages('RSQLite')
library(RSQLite)

install.packages('sqldf')
library(sqldf)

#------Text mining / Naive base------
install.packages('tm')
library(tm)

install.packages('e1071')
library(e1071)

install.packages('wordcloud')
library(wordcloud)

install.packages('SDMTools')
library(SDMTools)

#------Decision trees------
install.packages('rpart')
library(rpart)

install.packages('rpart.plot')
library(rpart.plot)

install.packages('ISLR')
library('ISLR')

#------Other Useful------
install.packages(c("caret","quanteda","irlba", "randomForetst"))
library('caret')
library('quanteda')
library('irlba')
library('randomForetst')


#----Q1 dtm

test <- read.csv('test.csv', stringsAsFactors = FALSE)

str(test)

#packag for dtm
install.packages('tm')
library(tm)

#change the category attribute into a factor
test$category<- as.factor(test$category)
str(test)

#buid a corpus from the text part of the file
test_corpus <- Corpus(VectorSource(test$text))


#build DTM 
dtm <- DocumentTermMatrix(test_corpus)

#check dimensions of dtm
dim(dtm)

#clean the corpus
clean_corpus <- tm_map(test_corpus,removePunctuation) #  ����� ����� �����
clean_corpus <- tm_map(clean_corpus,stripWhitespace)  # ����� ������ ������
clean_corpus <- tm_map(clean_corpus,removeNumbers)    # ����� ������
clean_corpus <- tm_map(clean_corpus,content_transformer(tolower)) #����� ������ ������ ������
clean_corpus <- tm_map(clean_corpus,removeWords, stopwords())   # ����� stopwords

#Buld DTM from the clean corpus 
dtm <- DocumentTermMatrix(clean_corpus)
dim(dtm)

#Reduce DT< columns to only frequent words
dtm_freq <-DocumentTermMatrix(clean_corpus,list(dictionary=findFreqTerms(dtm,4)))

dim(dtm_freq)
#���� 6 ������ ��������
inspect(dtm_freq[1:6, ])

#----Q2 Naive bayes
#convert the DTM to yes/no 

#Function to convert
conv_yesno <- function(x){
  x <- ifelse(x>0,1,0)
  x <- factor(x, level = c(1,0), labels = c('Yes', 'No'))
}

#Apply the function 
dtm.yn <- apply(dtm_freq, MARGIN = 1:2, conv_yesno)

#convert to data frame 
dtm.yn.df <- as.data.frame(dtm.yn)

#append the category colums 
df <- cbind(dtm.yn.df,test$category)

#Check structure 
str(df)

#run naive base 
install.packages('e1071')
library(e1071)

dim(df)

model.naive <- naiveBayes(df[,-18],df$`test$category`)


#Q3 --- Confusion matrix 
#Generating prediction vector 

predicted <- predict(model.naive,df)

table(predicted,df$`test$category`)

#Q4 --- logistic regression

#in logistic regression we need numeric values 

dtm_freq 

dtm <- as.matrix(dtm_freq)

dtm.df <- as.data.frame(dtm)

df.log <- cbind(dtm.df,test$category)

#change name of the category colums 
df.log$category <- df.log$`test$category`
df.log$`test$category` <- NULL 
str(df.log)

#generate the logistic regression model 

model.log <- glm(category ~ ., family = binomial(link = 'logit'), data = df.log)

summary(model.log)

#Q5 --- lofistic regression 

#prediction.log <- predict(model.log)

predicted.probabilities <- predict(model.log, newdata = df.log, type = 'response')

#change to 01 
predicted.01 <- ifelse (predicted.probabilities>0.5,1,0)

table(predicted.01, df.log$category)


#Q6--k-means

df.cluster <- df.log[,-18] 
str(df.cluster)

#Rnning k-means 
testClaster <- kmeans(df.cluster, 2, nstart = 20)

#Confusion matrix 
table(testClaster$cluster, df.log$category)



